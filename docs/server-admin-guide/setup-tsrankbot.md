# Setting up a new TSRankBot instance
## Prerequisites
While it is possible to run our rank-bot without using docker, we currently have no extra build-chain for that. So docker is a requirement.
## Prepare your teamspeak
### Setup a query account
Our bot needs a way to communicate with teamspeak. We use the query API for that.
So we have to create a new user.

It's recommended to use a new teamspeak ID for that which is not used by any other user on the server. This is because teamspeak binds query user to the profile which create the query user. Therefore you would get messages that are intended for the query user only if you create the user with your own account.

1. Connect to teamspeak with a new identity specifically for the bot.
1. go to `Tools` -> `ServerQuery login`.
1. Give it a name like `RankBot` and press `Ok`.
1. Now copy the credentials you see in the dialog and note them down.

### Assign required permissions to the QueryAccount
Now that we have create a new account for the bot and connected to the serve once, we need to assign a few permissions which are required by the bot to function.

TODO
