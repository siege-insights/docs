# Welcome to Siege-Insights
The goal of our project is to provide a better experience for players in communities.
We'd like to offer you a stats-tracking-system which suites your needs more directly and is updating statistics blazing fast.

## How we archive this
Existing websites may offer a lot of details, but they lack fast update and can't offer great integrations like a teamspeak bot that updates your personal rank every time it gets changed.

Therefore, we've written our own ecosystem for tracking stats.
There a multiple components which together make our "system" - Called Siege-Insights.

### The issue with the statistics
Ubisoft may offer a lot of statistics for Rainbow Six: Siege, but these are always just representing the **current** situation and **not** the progress / history.
This is where we come into place and create the missing details out of the data which we update frequently.

### What's the scope?
Whenever you play Siege, chances are high you do so with the same people from the same community / clan over and over again.
So we want to give you a way to compare yourself to your nearest friends, without checking it manually.

This starts by adding an integration to teamspeak (discord may follow later) which shows your current rank right next to the name in Teamspeak.

This will make it a lot easier for you and your friends to find matching player-partner.

Next up is a webpage with more details. If you're as curious as we about statistics, **you will love this page!**

The page gives you the ability to compare your stats directly with other players on the teamspeak and also allows your to dig into detailed stats.

There is also an internal leaderboard where you can challenge your friends from your community.
!!!info
    If you want get a first impression, have a look at our [Statistics-Page](https://stats.siege-insights.net)

